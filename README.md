# NAME

nayme - a Fantasy Name Generator

# VERSION

The version can be retrieved with option `--version`:

    $ nayme --version

# USAGE

    nayme [--help] [--man] [--usage] [--version]

    nayme [--count|-c|-n <int>]
          [--list|-l]
          [--specification|--spec|-s <string>]
          [--template|-t <name>]
          [--templates-from|-f <path>]

# EXAMPLES

    # list available templates (only internal ones)
    nayme --list

    # list available templates (internal & in file "definitions.json")
    nayme -f definitions.json -l

    # generate 20 fake "Greek" names 
    nayme --count 20 --template GREEK_NAMES

    # generate 1 fantasy name according to the specification
    nayme -s "BV(|||'|-)Cs"

# DESCRIPTION

This is a clone of the Perl version of
[https://github.com/skeeto/fantasyname](https://github.com/skeeto/fantasyname), which in turn is an attempt at
cloning the Fantasy Name Generator at [http://www.rinkworks.com/namegen/](http://www.rinkworks.com/namegen/).

The generator can use specifications provide by `--specification` and its
equivalents, as well as leverage upon pre-defined specifications by their name
provided via option `--template` or equivalments. It is possible to generate
many _similar_ names with option `--count`.

To know the list of available templates, it's possible to use option `--list`;
it's also possible to set a file with alternative templates, via option
`--templates-from` or its alieases.

# OPTIONS

- **--count|-c|-n &lt;int>**

    set the number of names to generate.

- **--help**

    print out some help and exit.

- **--list|-l**

    print the list of available pre-defined templates.

- **--man**

    show the manual page for nayme.

- **--specification|--spec|-s &lt;string>**
- **environment: NAYME\_SPECIFICATION**

    set the specification for generating the names.

- **--template|-t &lt;string>**
- **environment: NAYME\_TEMPLATE**

    name of the template to use for name specification.

- **--templates-from|-f &lt;path>**
- **environment: NAYME\_TEMPLATES\_FILE**

    name of a JSON-formatted file with additional templates of specifications.

- **--usage**

    show usage instructions.

- **--version**

    show version.

# CONFIGURATION

It is possible to provide a JSON-formatted file with a single object of
key/value pairs, where keys are template names and values are specifications
for generating names.

Example:

    {
       "simple": "<s|B|Bv|v><V|s|'|V><s|V|C>",
       "idiot":  "<i|Cd>D<d|i>",
       "short":  "<V|B><V|vs|Vs>"
    }

# DIAGNOSTICS

- **unknown template named <%s>**

    the template specified with `--template` (or via equivalent command line
    options or environment variable) is not present in the internal cache of the
    script or in the provided JSON file.

- **unknown sequence starting at %d <%s>**

    the specification contains sub-sequences that cannot be parsed.

- **nothing parsed**

    the specification does not contain anything.

# DEPENDENCIES

Perl 5.24 or later.

# BUGS AND LIMITATIONS

Please report any bugs or feature requests through the repository at
[https://gitlab.com/polettix/nayme](https://gitlab.com/polettix/nayme).

# AUTHOR

Flavio Poletti.

Embedded specifications/templates, as well as possible expansion for template
fragments, by Christopher Wellons and others
([https://github.com/skeeto/fantasyname](https://github.com/skeeto/fantasyname)).

# LICENSE AND COPYRIGHT

Copyright 2020 by Flavio Poletti `flavio@polettix.it`.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

or look for file `LICENSE` in this project's root directory.

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
